import * as handlers from './importer.mjs';

/**
 * Configuration of the adventure being exported/imported
 */
export const ADVENTURE = {
  // Basic information about the module and the adventure compendium it contains
  moduleName: 'ag-tykvennyj-festival',
  packName: 'tykvennyj-festival',
  packId: 'ag-tykvennyj-festival.tykvennyj-festival',
  adventureUuid:
    'Compendium.ag-tykvennyj-festival.tykvennyj-festival.Adventure.h3dd3D5xL8KarLiK',
  adventureId: 'h3dd3D5xL8KarLiK',
  changelogId: 'Mnkf7IdHIdKoqaNh',

  // A CSS class to automatically apply to application windows which belong to this module
  cssClass: 'ag-tykvennyj-festival',

  description: `Небольшая деревушка Лонгфог в очередной раз принимает у себя гостей в честь ежегодного праздника сбора урожая. Веселые конкурсы, вкусные угощения и бродячие артисты будут радовать весь день посетителей фестиваля. Однако староста обеспокоен за безопасность мероприятия. Последний месяц таинственный зверь терроризировал местных жителей, похищая домашнюю птицу, скот и даже людей...`,

  // Define special Import Options with custom callback logic
  importOptions: {
    displayJournal: {
      label: 'Открыть текст приключения',
      default: true,
      handler: handlers.displayJournal,
      documentId: 'qWYR8IX55ONi6mrv',
    },
    customizeJoin: {
      label: 'Преобразовать игровой мир',
      default: false,
      handler: handlers.customizeJoin,
      background: 'modules/ag-tykvennyj-festival/assets/ui/adventure-cover.webp',
    },
  },
};
