import AgTykvennyjFestival from './journal-sheet.mjs';
import {
  handleImportControls,
  onImport,
  renderAdventureImporter,
} from './adventure/importer.mjs';
import { ADVENTURE } from './adventure/adventure.mjs';

/* -------------------------------------------- */
/*  Hooks                                       */
/* -------------------------------------------- */

Hooks.on('init', () => {
  // const module = globalThis['ag-tykvennyj-festival'] = game.modules.get("ag-tykvennyj-festival");

  // Register Journal Sheet
  DocumentSheetConfig.registerSheet(
    JournalEntry,
    'ag-tykvennyj-festival',
    AgTykvennyjFestival,
    {
      types: ['base'],
      label: '🎃 Тыквенный фестиваль',
      makeDefault: false,
    },
  );
});

Hooks.on('ready', async () => {
  const imported = !!game.settings.get('core', 'adventureImports')?.[
    ADVENTURE.adventureUuid
  ];
  if (!imported && game.user.isGM) {
    const pack = game.packs.get(ADVENTURE.packId);
    const adventure = await pack.getDocument(ADVENTURE.adventureId);
    adventure.sheet.render(true);
  }
});

Hooks.on("renderChatMessage", (message, html, data) => {
    if (message.flags['ag-tykvennyj-festival-dice-game']) {
        html.find('.dice-formula').remove();
        requestAnimationFrame(() => {
            const imgElement = html.find("img");
            imgElement.attr("src", `modules/${ADVENTURE.moduleName}/assets/tokens/named/Dice%20Master%20Guren.webp`);
        }, 0)
    }
});

/* -------------------------------------------- */
/*  Adventure Import					                  */
/* -------------------------------------------- */

Hooks.on('renderAdventureImporter', renderAdventureImporter);
Hooks.on('preImportAdventure', handleImportControls);
Hooks.on('importAdventure', onImport);
