/**
 * Custom journal sheet for displaying navigation between pages.
 */
export default class AgTykvennyjFestival extends dnd5e.applications.journal
  .JournalSheet5e {
  constructor(doc, options) {
    super(doc, options);
    this.options.classes.push('ag-tykvennyj-festival');
  }

  /* Click Overrides */
  /** @inheritDoc */
  activateListeners(html) {
    super.activateListeners(html);

    html.on('click', (event) => {
      if (event.target.dataset.type === 'Scene') {
        event.stopImmediatePropagation();
        game.scenes.get(event.target.dataset.id).view();
      }
    });
  }

  /* --------------------------------------------- */
  /*  Rendering                                    */
  /* --------------------------------------------- */

  /** @inheritdoc */
  async _render(...args) {
    await super._render(...args);
    const [html] = this._element;
    const header = html.querySelector('.journal-entry-content .journal-header');

    html.querySelectorAll('[data-type="Scene"]').forEach((link) => {
      link.setAttribute('data-tooltip', 'Нажмите, чтобы перейти на сцену');
    });

    // Insert navigation
    const nav = this.document.getFlag('ag-tykvennyj-festival', 'navigation');
    if (nav) {
      const getDocument = (id) => {
        if (!this.document.pack) return game.journal.get(id);
        return game.packs.get(this.document.pack).getDocument(id);
      };
      const previous = nav.previous ? await getDocument(nav.previous) : null;
      const up = nav.up ? await getDocument(nav.up) : null;
      const next = nav.next ? await getDocument(nav.next) : null;
      header.insertAdjacentHTML(
        'afterend',
        `
        <nav class="book-navigation">
          <ul>
            <li>${
              previous
                ? `<a class="content-link" data-uuid="${previous.uuid}" rel="prev" data-tooltip="Предыдущий раздел" data-tooltip-direction="LEFT">${previous.name}</a>`
                : ''
            }</li>
            <li>${
              up
                ? `<a class="content-link parent" data-uuid="${up.uuid}" data-tooltip="Родительский журнал">${up.name}</a>`
                : ''
            }</li>
            <li>${
              next
                ? `<a class="content-link" data-uuid="${next.uuid}" rel="next" data-tooltip="Следующий раздел" data-tooltip-direction="RIGHT">${next.name}</a>`
                : ''
            }</li>
          </ul>
        </nav>
      `,
      );
    }
  }
}
